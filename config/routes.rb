Rails.application.routes.draw do
  
  resources :nuevos
  
  resources :zombies do
      resources :brains
  end      
  get "sexto_repo/cerebros", to: 'brains#index', as: 'brains'    
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
